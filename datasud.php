<?php
/*
Plugin Name:	DataSud
Description:	Intégration du catalogue de données CKAN
Version:		1.0.0
Author:			Mairie de Digne-les-Bains
License:		GPL-2.0+
License URI:	http://www.gnu.org/licenses/gpl-2.0.txt
This plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.
This plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with This plugin. If not, see {URI to Plugin License}.
*/



function addRessources() {


	wp_enqueue_style( 'datasud-style',  plugins_url( '/static/css/main.4ab23f7c.chunk.css' , __FILE__ ) );
	wp_enqueue_style( 'datasud-style-app',  plugins_url( '/app.css' , __FILE__ ) );


	
	wp_enqueue_script( 'datasud-script', plugins_url( 'static/js/main.e7d5e4c0.chunk.js' , __FILE__ ), array(), '1.0.0', true );
	wp_enqueue_script( 'datasud-js-2.a881f2a4', plugins_url( 'static/js/2.a881f2a4.chunk.js' , __FILE__ ), array(), '1.0.0', true );
	wp_enqueue_script( 'datasud-js-runtime', plugins_url( 'static/js/runtime-main.60db20b2.js' , __FILE__ ), array(), '1.0.0', true );

	wp_enqueue_script( 'datasud-script-cfg', plugins_url( 'static/js/datasud-cfg.js' , __FILE__ ), array(), '1.0.0', true );


}

add_shortcode('datasud', 'datasud');


function datasud() {

	addRessources();

	return "<div id='ckan-widget'></div>";
}
?>
