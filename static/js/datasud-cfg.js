var config = {
    // URL du catalogue CKAN cible
    ckan_api: 'https://XXX',
    // Filtres complémentaires optionnels :
    ckan_organizations: ['XXX'],
    //ckan_organizations: ['org1', 'org2'],
    //ckan_groups: ['group1'],
    //ckan_tags: ['tag1'],
    /*ckan_facets: {
        res_format: 'HTML',
        datatype: 'type'
    },*/

    // paramétrages de l'affichage :
    data_sort: 'title_string asc',
    result_page_size: 10,
    thumbnails_display: true
}


ckanWidget.init(config)
